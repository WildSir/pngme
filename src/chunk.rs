use crc::{Crc, CRC_32_ISO_HDLC};
use thiserror::Error;

use crate::chunk_type::{ChunkType, ChunkTypeError};

use std::{fmt::Display, string::FromUtf8Error};
#[derive(Debug)]

pub struct Chunk {
    chunk_type: ChunkType,
    data: Vec<u8>,
}

impl Chunk {
    pub fn new(chunk_type: ChunkType, data: Vec<u8>) -> Self {
        Chunk { chunk_type, data }
    }
    pub fn length(&self) -> u32 {
        self.data.len() as u32
    }
    pub fn chunk_type(&self) -> &ChunkType {
        &self.chunk_type
    }
    pub fn data(&self) -> &[u8] {
        &self.data
    }
    pub fn crc(&self) -> u32 {
        let crc = Crc::<u32>::new(&CRC_32_ISO_HDLC);
        crc.checksum(
            &self
                .chunk_type()
                .bytes()
                .iter()
                .chain(self.data())
                .copied()
                .collect::<Vec<u8>>(),
        )
    }
    pub fn data_as_string(&self) -> Result<String, FromUtf8Error> {
        String::from_utf8(self.data().to_vec())
    }
    pub fn as_bytes(&self) -> Vec<u8> {
        [
            &self.length().to_be_bytes(),
            &self.chunk_type().bytes(),
            self.data(),
            &self.crc().to_be_bytes(),
        ]
        .concat()
        // Alternative method because this feels like cheating: vec1.iter().chain(vec2.iter()).chain(vec3.iter()).collect::<Vec<_>>()
    }
}

#[derive(Error, Debug)]
pub enum ChunkError {
    #[error("wrong chunk length; must be at least 12 bytes long")]
    WrongLength,
    #[error("error with chunk type parsing")]
    ChunkTypeError(#[from] ChunkTypeError),
    #[error("crc does not match the data")]
    InvalidCrc,
    #[error("failed to parse chunk")]
    FailedParsing,
}
impl From<Vec<u8>> for ChunkError {
    fn from(_value: Vec<u8>) -> Self {
        ChunkError::FailedParsing
    }
}
// impl TryFrom<&[u8]> for Chunk {
//     type Error = ChunkError;
//     fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
//         let mut iter = value.iter();
//         // NO GUARENTEE FOR TAKE
//         if value.len() < 12 {
//             return Err(ChunkError::WrongLength);
//         }
//         let length: [u8; 4] = iter.by_ref().take(4).try_into().unwrap();
//         let chunk_type = ChunkType::try_from(TryInto::<[u8; 4]>::try_into(iter.by_ref().take(4).copied().collect::<Vec<_>>()).unwrap())?;
//         let data: Vec<u8> = iter.by_ref().take(u32::from_be_bytes(length.try_into().unwrap()) as usize).copied().collect();
//         let _crc: Vec<u8> = iter.by_ref().take(4).copied().collect();
//         let chunk = Chunk::new(chunk_type, data);
//         todo!()
//     }
// }
impl TryFrom<&[u8]> for Chunk {
    type Error = ChunkError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        if value.len() < 12 {
            return Err(ChunkError::WrongLength);
        }

        let mut iter = value.iter().copied();

        let length_bytes: [u8; 4] = iter.by_ref().take(4).collect::<Vec<u8>>().try_into()?;
        let length: usize = u32::from_be_bytes(length_bytes).try_into().unwrap();

        let chunk_type_bytes: [u8; 4] = iter.by_ref().take(4).collect::<Vec<u8>>().try_into()?;
        let chunk_type = ChunkType::try_from(chunk_type_bytes)?;

        let data: Vec<u8> = iter.by_ref().take(length).collect();
        let crc_bytes: [u8; 4] = iter.by_ref().take(4).collect::<Vec<u8>>().try_into()?;

        let crc_data: Vec<u8> = chunk_type_bytes.iter().chain(&data).copied().collect();
        let crc = Crc::<u32>::new(&CRC_32_ISO_HDLC);
        if crc.checksum(&crc_data) != u32::from_be_bytes(crc_bytes) {
            return Err(ChunkError::InvalidCrc);
        }

        let chunk = Chunk::new(chunk_type, data);

        Ok(chunk)
    }
}

impl Display for Chunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Chunk Type: {}", self.chunk_type().to_string())?;
        writeln!(f, "Data Length: {}", self.length())?;
        writeln!(
            f,
            "Data: {}",
            String::from_utf8(self.data().to_vec()).unwrap_or(String::from("Cannot be displayed"))
        )?;
        writeln!(f, "CRC: {}", self.crc())?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::chunk_type::ChunkType;
    use std::str::FromStr;

    fn testing_chunk() -> Chunk {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        Chunk::try_from(chunk_data.as_ref()).unwrap()
    }

    #[test]
    fn test_new_chunk() {
        let chunk_type = ChunkType::from_str("RuSt").unwrap();
        let data = "This is where your secret message will be!"
            .as_bytes()
            .to_vec();
        let chunk = Chunk::new(chunk_type, data);
        assert_eq!(chunk.length(), 42);
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_chunk_length() {
        let chunk = testing_chunk();
        assert_eq!(chunk.length(), 42);
    }

    #[test]
    fn test_chunk_type() {
        let chunk = testing_chunk();
        assert_eq!(chunk.chunk_type().to_string(), String::from("RuSt"));
    }

    #[test]
    fn test_chunk_string() {
        let chunk = testing_chunk();
        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");
        assert_eq!(chunk_string, expected_chunk_string);
    }

    #[test]
    fn test_chunk_crc() {
        let chunk = testing_chunk();
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_valid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref()).unwrap();

        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");

        assert_eq!(chunk.length(), 42);
        assert_eq!(chunk.chunk_type().to_string(), String::from("RuSt"));
        assert_eq!(chunk_string, expected_chunk_string);
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_invalid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656333;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref());

        assert!(chunk.is_err());
    }

    #[test]
    pub fn test_chunk_trait_impls() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk: Chunk = TryFrom::try_from(chunk_data.as_ref()).unwrap();

        let _chunk_string = format!("{}", chunk);
    }
}
