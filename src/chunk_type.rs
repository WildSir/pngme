use std::{fmt::Display, str::FromStr};

use thiserror::Error;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct ChunkType {
    ancillary_bit: u8,
    private_bit: u8,
    reserved_bit: u8,
    safe_to_copy_bit: u8,
}

impl ChunkType {
    pub fn bytes(&self) -> [u8; 4] {
        [
            self.ancillary_bit,
            self.private_bit,
            self.reserved_bit,
            self.safe_to_copy_bit,
        ]
    }
    pub fn is_valid(&self) -> bool {
        self.is_reserved_bit_valid() && self.bytes().iter().all(|b| b.is_ascii_alphabetic())
    }
    pub fn is_critical(&self) -> bool {
        self.ancillary_bit.is_ascii_uppercase()
    }
    pub fn is_public(&self) -> bool {
        self.private_bit.is_ascii_uppercase()
    }
    pub fn is_reserved_bit_valid(&self) -> bool {
        self.reserved_bit.is_ascii_uppercase()
    }
    pub fn is_safe_to_copy(&self) -> bool {
        self.safe_to_copy_bit.is_ascii_lowercase()
    }
}

#[derive(Error, Debug)]
pub enum ChunkTypeError {
    #[error("all bytes must be ascii alphabetic characters")]
    NotAsciiAlphabetic,
    #[error("png chunk types must be exactly 4 bytes long")]
    IncorrectLength,
}

impl TryFrom<[u8; 4]> for ChunkType {
    type Error = ChunkTypeError;
    fn try_from(value: [u8; 4]) -> Result<Self, Self::Error> {
        if !value.iter().all(|v| v.is_ascii_alphabetic()) {
            Err(ChunkTypeError::NotAsciiAlphabetic)
        } else {
            Ok(ChunkType {
                ancillary_bit: value[0],
                private_bit: value[1],
                reserved_bit: value[2],
                safe_to_copy_bit: value[3],
            })
        }
    }
}

impl FromStr for ChunkType {
    type Err = ChunkTypeError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(bytes) = TryInto::<[u8; 4]>::try_into(s.as_bytes()) {
            Ok(ChunkType::try_from(bytes)?)
        } else {
            Err(ChunkTypeError::IncorrectLength)
        }
    }
}

impl Display for ChunkType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", String::from_utf8(self.bytes().to_vec()).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryFrom;
    use std::str::FromStr;

    #[test]
    pub fn test_chunk_type_from_bytes() {
        let expected = [82, 117, 83, 116];
        let actual = ChunkType::try_from([82, 117, 83, 116]).unwrap();

        assert_eq!(expected, actual.bytes());
    }

    #[test]
    pub fn test_chunk_type_from_str() {
        let expected = ChunkType::try_from([82, 117, 83, 116]).unwrap();
        let actual = ChunkType::from_str("RuSt").unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    pub fn test_chunk_type_is_critical() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_critical());
    }

    #[test]
    pub fn test_chunk_type_is_not_critical() {
        let chunk = ChunkType::from_str("ruSt").unwrap();
        assert!(!chunk.is_critical());
    }

    #[test]
    pub fn test_chunk_type_is_public() {
        let chunk = ChunkType::from_str("RUSt").unwrap();
        assert!(chunk.is_public());
    }

    #[test]
    pub fn test_chunk_type_is_not_public() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(!chunk.is_public());
    }

    #[test]
    pub fn test_chunk_type_is_reserved_bit_valid() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_reserved_bit_valid());
    }

    #[test]
    pub fn test_chunk_type_is_reserved_bit_invalid() {
        let chunk = ChunkType::from_str("Rust").unwrap();
        assert!(!chunk.is_reserved_bit_valid());
    }

    #[test]
    pub fn test_chunk_type_is_safe_to_copy() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_safe_to_copy());
    }

    #[test]
    pub fn test_chunk_type_is_unsafe_to_copy() {
        let chunk = ChunkType::from_str("RuST").unwrap();
        assert!(!chunk.is_safe_to_copy());
    }

    #[test]
    pub fn test_valid_chunk_is_valid() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_valid());
    }

    #[test]
    pub fn test_invalid_chunk_is_valid() {
        let chunk = ChunkType::from_str("Rust").unwrap();
        assert!(!chunk.is_valid());

        let chunk = ChunkType::from_str("Ru1t");
        assert!(chunk.is_err());
    }

    #[test]
    pub fn test_chunk_type_string() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert_eq!(&chunk.to_string(), "RuSt");
    }

    #[test]
    pub fn test_chunk_type_trait_impls() {
        let chunk_type_1: ChunkType = TryFrom::try_from([82, 117, 83, 116]).unwrap();
        let chunk_type_2: ChunkType = FromStr::from_str("RuSt").unwrap();
        let _chunk_string = format!("{}", chunk_type_1);
        let _are_chunks_equal = chunk_type_1 == chunk_type_2;
    }
}
