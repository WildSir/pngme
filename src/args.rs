use clap::Parser;
use thiserror::Error;

use std::fs;

use crate::chunk::Chunk;
use crate::commands::Commands;
use crate::png::Png;
use crate::Result;

#[derive(Parser, Debug)]
pub struct Args {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Error, Debug)]
#[error("the specified chunk type did not match any chunk")]
pub struct ChunkNotFound;

pub fn parse_args(args: Args) -> Result<()> {
    match args.command {
        Commands::Encode {
            file_path,
            chunk_type,
            message,
            output_file,
        } => {
            let file = fs::read(&file_path)?;
            let mut png = Png::try_from(file.as_slice())?;
            let chunk = Chunk::new(chunk_type, message.as_bytes().to_vec());
            png.append_chunk(chunk);
            if let Some(output_file) = output_file {
                fs::write(output_file, png.as_bytes())?;
            } else {
                fs::write(&file_path, png.as_bytes())?;
            }
        }
        Commands::Decode {
            file_path,
            chunk_type,
        } => {
            let file = fs::read(&file_path)?;
            let png = Png::try_from(file.as_slice())?;
            if let Some(chunk) = png.chunk_by_type(&chunk_type.to_string()) {
                match chunk.data_as_string() {
                    Ok(data) => println!("Chunk Data: {}", data),
                    Err(_) => eprintln!("Chunk data is not valid UTF-8."),
                }
            } else {
                return Err(Box::new(ChunkNotFound));
            }
        }
        Commands::Remove {
            file_path,
            chunk_type,
        } => {
            let file = fs::read(&file_path)?;
            let mut png = Png::try_from(file.as_slice())?;
            if let Some(_) = png.remove_chunk(&chunk_type.to_string()) {
                println!("Chunk removed");
                fs::write(&file_path, png.as_bytes())?;
            } else {
                return Err(Box::new(ChunkNotFound));
            }
        }
        Commands::Print { file_path } => {
            let file = fs::read(file_path)?;
            let png = Png::try_from(file.as_slice())?;
            println!("{png}");
        }
    }
    Ok(())
}
