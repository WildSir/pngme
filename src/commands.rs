use std::path::PathBuf;

use clap::Subcommand;

use crate::chunk_type::ChunkType;

#[derive(Debug, Subcommand)]
pub enum Commands {
    Encode {
        file_path: PathBuf,
        chunk_type: ChunkType,
        message: String,
        #[arg(last = true)]
        output_file: Option<String>,
    },
    Decode {
        file_path: PathBuf,
        chunk_type: ChunkType,
    },
    Remove {
        file_path: PathBuf,
        chunk_type: ChunkType,
    },
    Print {
        file_path: PathBuf,
    },
}
